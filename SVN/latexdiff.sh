#!/bin/bash
# Script to remove all -L arguments since latexdiff does not understand them
# and they carry no meaning for processing, only so the user can identify the
# versions of the files that are compared
args=()
while [ $# -gt 0 ]
do
	arg=$1
	if [ $arg = "-L" ]
	then
		shift
		echo "% latexdiff processing file: $1"
	else
		args=("${args[@]}" "$arg")
	fi
	shift
done
latexdiff ${args[@]}
