#!/bin/bash

##
# This is an install script that will set up your hg/git environment properly
# You need to have latexdiff installed, together with pdflatex

#    This script is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

function usage {
    echo "Usage: "
    echo "      $0 [-h -c -r <git/hg> -p <prefix>]"
    echo "Options: "
    echo ' h: print this message'
    echo ' r: define rcs of choice (git or hg)'
    echo ' p: define prefix (default $HOME/.local/)'
    echo ' c: do not configure RCS (not recommended)'
    echo 'All options are optional...'
    exit 1
}
if test $? != 0
     then
         usage
fi
conf=true
while getopts  "hp:r:c" flag
do
  case "$flag" in
    h) usage;;
    p) export prefix=$OPTARG;;
    r) export env=$OPTARG;;
    c) export conf=false;;
  esac
done

if [ ! $prefix ]
then
export prefix="$HOME/.local"
fi
if [ ! $env ]
then
    echo 'Do you use Mercurial or Git? (answer hg/git)'
    read env
fi


if [[ $env != hg && $env != git ]]
 then
  echo "$env Not a valid choice for revision control system"
  usage
fi



#creating $prefix/bin/ and adding it to path:
if [ ! -d $prefix/bin/ ]
then
 mkdir -p $prefix/bin
fi

prefixbin=`echo ${prefix}/bin | sed s#//#/#g`

if ! echo $PATH | grep "${prefixbin}" 1>/dev/null 2>&1
then
    echo -e "\n\nPlease make sure that $prefixbin is in your" '$PATH'
    echo -e 'Normally that would mean adding a line like this:'
    echo -e "\t PATH=$prefix/bin:"'$PATH\n'
    echo -e 'to your `$HOME/.profile` file.\n'
fi


if [[ $env == git ]]
then
    if [[ "$OSTYPE" =~ "linux" ]]
    then #linux-gnu
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='git difftool -t latex --no-prompt'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexgitdiff/g" > latexdiff-git
    elif [[ "$OSTYPE" =~ "arwin" ]] # osx
    then
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='git difftool -t latex --no-prompt'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexgitdiff/g" | sed "s/xdg-open/open/g" > latexdiff-git
    else #unknown os
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='git difftool -t latex --no-prompt'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexgitdiff/g" > latexdiff-git
    fi
    if $conf
    then
        prevcmd=`git config --get difftool.latex.cmd`
        newcmd='latexdiff --exclude-textcmd="chapter,section,subsection,subsubsection" "$LOCAL" "$REMOTE"'
        if [ $? -eq 0 ] && [ "$prevcmd" != "$newcmd" ]
        then
            echo "Want to create new command for difftool.latex"
            echo "This will overwrite the existing command, is this OK? (y/n)"
            read ans
            if [ $ans == y ] || [ $ans == yes ]
            then
                git config --global difftool.latex.cmd 'latexdiff  "$LOCAL" "$REMOTE"'
            else
                echo "WARNING: The script will not work without the difftool.latex command correctly set up"
            fi
        elif [ $? -eq 1 ] # means command did not exist
        then
            git config --global difftool.latex.cmd $newcmd
        fi
    fi

    echo "will now install script in $prefix/bin/"
    chmod +x latexdiff-git
    mv latexdiff-git $prefix/bin/
    if [ -f $prefix/bin/latexdiff-git ]
    then
        echo 'Installation finished. The script is called latexdiff-git'
    fi
fi


if [[ $env == hg ]]
then
    if [[ "$OSTYPE" =~ "linux-gnu" ]]
    then #linux-gnu
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='hg latexdiff'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexhgdiff/g" > latexdiff-hg
    elif [[ "$OSTYPE" =~ "arwin" ]] #osx
    then
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='hg latexdiff'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexhgdiff/g" | sed "s/xdg-open/open/g" > latexdiff-hg
    else #unknown os
        sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='hg latexdiff'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexhgdiff/g" > latexdiff-hg
    fi
    
    sed  -e "s/revlatexdiffcmd='please define'/revlatexdiffcmd='hg latexdiff'/" latexbatchdiff.sh | sed "s/latexbatchdiff/latexhgdiff/g" > latexdiff-hg
    
    if $conf
    then
    touch $HOME/.hgrc
        if grep -q "cmd.latexdiff = latexdiff" $HOME/.hgrc
        then
            echo 'Did you already add the latex extension to ~/.hgrc? (y/n)'
            read ans
        if [[ $ans == y ]]
        then
            echo 'Assuming you have it "correctly" installed, so that this script works'
        else
            echo 'Installation failed'
            echo 'Could not add extension to your hgrc'
            exit 0
        fi
        else
            if ! grep -q "extdiff =" $HOME/.hgrc
            then
                echo -e '[extensions]\n extdiff =\n' >> $HOME/.hgrc
            fi
            echo -e '[extdiff]\n cmd.latexdiff = latexdiff' >> $HOME/.hgrc
        fi
    fi

    echo "will now install script in $prefix/bin/"
    chmod +x latexdiff-hg
    mv latexdiff-hg $prefix/bin/
    if [ -f $prefix/bin/latexdiff-hg ]
    then
        echo 'installation finished. the script is called latexdiff-hg'
    fi
fi
